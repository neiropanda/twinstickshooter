// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TESTPROJECTARTCRAFT_BaseWeapon_generated_h
#error "BaseWeapon.generated.h already included, missing '#pragma once' in BaseWeapon.h"
#endif
#define TESTPROJECTARTCRAFT_BaseWeapon_generated_h

#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_9_DELEGATE \
static inline void FNoParamDelegate_DelegateWrapper(const FMulticastScriptDelegate& NoParamDelegate) \
{ \
	NoParamDelegate.ProcessMulticastDelegate<UObject>(NULL); \
}


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execPlayFireSound) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->PlayFireSound(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlayReloadSound) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->PlayReloadSound(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlayReloadMontage) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->PlayReloadMontage(); \
		P_NATIVE_END; \
	}


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execPlayFireSound) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->PlayFireSound(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlayReloadSound) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->PlayReloadSound(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlayReloadMontage) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->PlayReloadMontage(); \
		P_NATIVE_END; \
	}


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABaseWeapon(); \
	friend struct Z_Construct_UClass_ABaseWeapon_Statics; \
public: \
	DECLARE_CLASS(ABaseWeapon, ASkeletalMeshActor, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(ABaseWeapon)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_17_INCLASS \
private: \
	static void StaticRegisterNativesABaseWeapon(); \
	friend struct Z_Construct_UClass_ABaseWeapon_Statics; \
public: \
	DECLARE_CLASS(ABaseWeapon, ASkeletalMeshActor, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(ABaseWeapon)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABaseWeapon(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABaseWeapon) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABaseWeapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABaseWeapon); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABaseWeapon(ABaseWeapon&&); \
	NO_API ABaseWeapon(const ABaseWeapon&); \
public:


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABaseWeapon(ABaseWeapon&&); \
	NO_API ABaseWeapon(const ABaseWeapon&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABaseWeapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABaseWeapon); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABaseWeapon)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bIsSprinting() { return STRUCT_OFFSET(ABaseWeapon, bIsSprinting); } \
	FORCEINLINE static uint32 __PPO__MuzzleSocketName() { return STRUCT_OFFSET(ABaseWeapon, MuzzleSocketName); } \
	FORCEINLINE static uint32 __PPO__FireRate() { return STRUCT_OFFSET(ABaseWeapon, FireRate); } \
	FORCEINLINE static uint32 __PPO__bAutoFire() { return STRUCT_OFFSET(ABaseWeapon, bAutoFire); } \
	FORCEINLINE static uint32 __PPO__WeaponOwner() { return STRUCT_OFFSET(ABaseWeapon, WeaponOwner); } \
	FORCEINLINE static uint32 __PPO__ReloadSound() { return STRUCT_OFFSET(ABaseWeapon, ReloadSound); } \
	FORCEINLINE static uint32 __PPO__FireSound() { return STRUCT_OFFSET(ABaseWeapon, FireSound); } \
	FORCEINLINE static uint32 __PPO__ImpactComp() { return STRUCT_OFFSET(ABaseWeapon, ImpactComp); }


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_14_PROLOG
#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_17_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_17_RPC_WRAPPERS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_17_INCLASS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_17_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_17_INCLASS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<class ABaseWeapon>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseWeapon_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
