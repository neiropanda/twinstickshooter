// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TESTPROJECTARTCRAFT_PickUp_Ammo_generated_h
#error "PickUp_Ammo.generated.h already included, missing '#pragma once' in PickUp_Ammo.h"
#endif
#define TESTPROJECTARTCRAFT_PickUp_Ammo_generated_h

#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h_15_RPC_WRAPPERS
#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPickUp_Ammo(); \
	friend struct Z_Construct_UClass_APickUp_Ammo_Statics; \
public: \
	DECLARE_CLASS(APickUp_Ammo, ABasePickUp, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(APickUp_Ammo)


#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPickUp_Ammo(); \
	friend struct Z_Construct_UClass_APickUp_Ammo_Statics; \
public: \
	DECLARE_CLASS(APickUp_Ammo, ABasePickUp, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(APickUp_Ammo)


#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APickUp_Ammo(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APickUp_Ammo) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APickUp_Ammo); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APickUp_Ammo); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APickUp_Ammo(APickUp_Ammo&&); \
	NO_API APickUp_Ammo(const APickUp_Ammo&); \
public:


#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APickUp_Ammo() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APickUp_Ammo(APickUp_Ammo&&); \
	NO_API APickUp_Ammo(const APickUp_Ammo&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APickUp_Ammo); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APickUp_Ammo); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APickUp_Ammo)


#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h_15_PRIVATE_PROPERTY_OFFSET
#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h_12_PROLOG
#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h_15_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h_15_RPC_WRAPPERS \
	TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h_15_INCLASS \
	TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h_15_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h_15_INCLASS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<class APickUp_Ammo>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUp_Ammo_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
