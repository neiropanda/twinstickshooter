// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTestProjectArtCraft_init() {}
	TESTPROJECTARTCRAFT_API UFunction* Z_Construct_UDelegateFunction_TestProjectArtCraft_MyDelegate__DelegateSignature();
	TESTPROJECTARTCRAFT_API UFunction* Z_Construct_UDelegateFunction_TestProjectArtCraft_NoMyParamDelegate__DelegateSignature();
	TESTPROJECTARTCRAFT_API UFunction* Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate__DelegateSignature();
	TESTPROJECTARTCRAFT_API UFunction* Z_Construct_UDelegateFunction_TestProjectArtCraft_NoParamDelegate__DelegateSignature();
	TESTPROJECTARTCRAFT_API UFunction* Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate2__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_TestProjectArtCraft()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_TestProjectArtCraft_MyDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_TestProjectArtCraft_NoMyParamDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_TestProjectArtCraft_NoParamDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate2__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/TestProjectArtCraft",
				SingletonFuncArray,
				ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x28603FEA,
				0xAFD04A63,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
