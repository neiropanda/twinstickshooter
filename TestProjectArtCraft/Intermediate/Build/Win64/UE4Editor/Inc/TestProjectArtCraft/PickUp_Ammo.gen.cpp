// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TestProjectArtCraft/PickUp/PickUp_Ammo.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePickUp_Ammo() {}
// Cross Module References
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_APickUp_Ammo_NoRegister();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_APickUp_Ammo();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_ABasePickUp();
	UPackage* Z_Construct_UPackage__Script_TestProjectArtCraft();
// End Cross Module References
	void APickUp_Ammo::StaticRegisterNativesAPickUp_Ammo()
	{
	}
	UClass* Z_Construct_UClass_APickUp_Ammo_NoRegister()
	{
		return APickUp_Ammo::StaticClass();
	}
	struct Z_Construct_UClass_APickUp_Ammo_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Count;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APickUp_Ammo_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ABasePickUp,
		(UObject* (*)())Z_Construct_UPackage__Script_TestProjectArtCraft,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APickUp_Ammo_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PickUp/PickUp_Ammo.h" },
		{ "ModuleRelativePath", "PickUp/PickUp_Ammo.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APickUp_Ammo_Statics::NewProp_Count_MetaData[] = {
		{ "Category", "Ammo Crate" },
		{ "ModuleRelativePath", "PickUp/PickUp_Ammo.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_APickUp_Ammo_Statics::NewProp_Count = { "Count", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APickUp_Ammo, Count), METADATA_PARAMS(Z_Construct_UClass_APickUp_Ammo_Statics::NewProp_Count_MetaData, ARRAY_COUNT(Z_Construct_UClass_APickUp_Ammo_Statics::NewProp_Count_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APickUp_Ammo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APickUp_Ammo_Statics::NewProp_Count,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APickUp_Ammo_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APickUp_Ammo>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APickUp_Ammo_Statics::ClassParams = {
		&APickUp_Ammo::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_APickUp_Ammo_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_APickUp_Ammo_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APickUp_Ammo_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APickUp_Ammo_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APickUp_Ammo()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APickUp_Ammo_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APickUp_Ammo, 2480690517);
	template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<APickUp_Ammo>()
	{
		return APickUp_Ammo::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APickUp_Ammo(Z_Construct_UClass_APickUp_Ammo, &APickUp_Ammo::StaticClass, TEXT("/Script/TestProjectArtCraft"), TEXT("APickUp_Ammo"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APickUp_Ammo);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
