// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TestProjectArtCraft/Weapon/ImpactEffectActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeImpactEffectActor() {}
// Cross Module References
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_AImpactEffectActor_NoRegister();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_AImpactEffectActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_TestProjectArtCraft();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	ENGINE_API UClass* Z_Construct_UClass_UParticleSystem_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USoundBase_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
// End Cross Module References
	void AImpactEffectActor::StaticRegisterNativesAImpactEffectActor()
	{
	}
	UClass* Z_Construct_UClass_AImpactEffectActor_NoRegister()
	{
		return AImpactEffectActor::StaticClass();
	}
	struct Z_Construct_UClass_AImpactEffectActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EffectHit_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EffectHit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Damage_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Damage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImpulseStrength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ImpulseStrength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyImpulse_MetaData[];
#endif
		static void NewProp_bApplyImpulse_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyImpulse;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EffectParticle_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EffectParticle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BulletSound_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BulletSound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DecalMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DecalMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AImpactEffectActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_TestProjectArtCraft,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AImpactEffectActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Weapon/ImpactEffectActor.h" },
		{ "ModuleRelativePath", "Weapon/ImpactEffectActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_EffectHit_MetaData[] = {
		{ "ModuleRelativePath", "Weapon/ImpactEffectActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_EffectHit = { "EffectHit", nullptr, (EPropertyFlags)0x0020088000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AImpactEffectActor, EffectHit), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_EffectHit_MetaData, ARRAY_COUNT(Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_EffectHit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_Damage_MetaData[] = {
		{ "Category", "Damage" },
		{ "ModuleRelativePath", "Weapon/ImpactEffectActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_Damage = { "Damage", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AImpactEffectActor, Damage), METADATA_PARAMS(Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_Damage_MetaData, ARRAY_COUNT(Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_Damage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_ImpulseStrength_MetaData[] = {
		{ "Category", "Effects" },
		{ "EditCondition", "bApplyImpulse" },
		{ "ModuleRelativePath", "Weapon/ImpactEffectActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_ImpulseStrength = { "ImpulseStrength", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AImpactEffectActor, ImpulseStrength), METADATA_PARAMS(Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_ImpulseStrength_MetaData, ARRAY_COUNT(Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_ImpulseStrength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_bApplyImpulse_MetaData[] = {
		{ "Category", "Effects" },
		{ "ModuleRelativePath", "Weapon/ImpactEffectActor.h" },
	};
#endif
	void Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_bApplyImpulse_SetBit(void* Obj)
	{
		((AImpactEffectActor*)Obj)->bApplyImpulse = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_bApplyImpulse = { "bApplyImpulse", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AImpactEffectActor), &Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_bApplyImpulse_SetBit, METADATA_PARAMS(Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_bApplyImpulse_MetaData, ARRAY_COUNT(Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_bApplyImpulse_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_EffectParticle_MetaData[] = {
		{ "Category", "Effects" },
		{ "ModuleRelativePath", "Weapon/ImpactEffectActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_EffectParticle = { "EffectParticle", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AImpactEffectActor, EffectParticle), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_EffectParticle_MetaData, ARRAY_COUNT(Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_EffectParticle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_BulletSound_MetaData[] = {
		{ "Category", "Effects" },
		{ "ModuleRelativePath", "Weapon/ImpactEffectActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_BulletSound = { "BulletSound", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AImpactEffectActor, BulletSound), Z_Construct_UClass_USoundBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_BulletSound_MetaData, ARRAY_COUNT(Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_BulletSound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_DecalMaterial_MetaData[] = {
		{ "Category", "Effects" },
		{ "ModuleRelativePath", "Weapon/ImpactEffectActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_DecalMaterial = { "DecalMaterial", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AImpactEffectActor, DecalMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_DecalMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_DecalMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AImpactEffectActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_EffectHit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_Damage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_ImpulseStrength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_bApplyImpulse,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_EffectParticle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_BulletSound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AImpactEffectActor_Statics::NewProp_DecalMaterial,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AImpactEffectActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AImpactEffectActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AImpactEffectActor_Statics::ClassParams = {
		&AImpactEffectActor::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AImpactEffectActor_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_AImpactEffectActor_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_AImpactEffectActor_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AImpactEffectActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AImpactEffectActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AImpactEffectActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AImpactEffectActor, 209374547);
	template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<AImpactEffectActor>()
	{
		return AImpactEffectActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AImpactEffectActor(Z_Construct_UClass_AImpactEffectActor, &AImpactEffectActor::StaticClass, TEXT("/Script/TestProjectArtCraft"), TEXT("AImpactEffectActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AImpactEffectActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
