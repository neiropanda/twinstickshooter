// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TestProjectArtCraft/Weapon/RocketProjectile.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRocketProjectile() {}
// Cross Module References
	TESTPROJECTARTCRAFT_API UFunction* Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate2__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_TestProjectArtCraft();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_ARocketProjectile_NoRegister();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_ARocketProjectile();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UProjectileMovementComponent_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate2__DelegateSignature_Statics
	{
		struct _Script_TestProjectArtCraft_eventHitResultDelegate2_Parms
		{
			FHitResult RocketHit;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RocketHit;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate2__DelegateSignature_Statics::NewProp_RocketHit = { "RocketHit", nullptr, (EPropertyFlags)0x0010008000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_TestProjectArtCraft_eventHitResultDelegate2_Parms, RocketHit), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate2__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate2__DelegateSignature_Statics::NewProp_RocketHit,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate2__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Weapon/RocketProjectile.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate2__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_TestProjectArtCraft, nullptr, "HitResultDelegate2__DelegateSignature", sizeof(_Script_TestProjectArtCraft_eventHitResultDelegate2_Parms), Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate2__DelegateSignature_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate2__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate2__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate2__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate2__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate2__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	void ARocketProjectile::StaticRegisterNativesARocketProjectile()
	{
	}
	UClass* Z_Construct_UClass_ARocketProjectile_NoRegister()
	{
		return ARocketProjectile::StaticClass();
	}
	struct Z_Construct_UClass_ARocketProjectile_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnHitFireRocket_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_OnHitFireRocket;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RocketBullet_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RocketBullet;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RocketMovementComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RocketMovementComp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARocketProjectile_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_TestProjectArtCraft,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARocketProjectile_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Weapon/RocketProjectile.h" },
		{ "ModuleRelativePath", "Weapon/RocketProjectile.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARocketProjectile_Statics::NewProp_OnHitFireRocket_MetaData[] = {
		{ "ModuleRelativePath", "Weapon/RocketProjectile.h" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UClass_ARocketProjectile_Statics::NewProp_OnHitFireRocket = { "OnHitFireRocket", nullptr, (EPropertyFlags)0x0010000000080000, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARocketProjectile, OnHitFireRocket), Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate2__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_ARocketProjectile_Statics::NewProp_OnHitFireRocket_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARocketProjectile_Statics::NewProp_OnHitFireRocket_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARocketProjectile_Statics::NewProp_RocketBullet_MetaData[] = {
		{ "Category", "RocketProjectile" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Weapon/RocketProjectile.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARocketProjectile_Statics::NewProp_RocketBullet = { "RocketBullet", nullptr, (EPropertyFlags)0x00200800000a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARocketProjectile, RocketBullet), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARocketProjectile_Statics::NewProp_RocketBullet_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARocketProjectile_Statics::NewProp_RocketBullet_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARocketProjectile_Statics::NewProp_RocketMovementComp_MetaData[] = {
		{ "Category", "RocketProjectile" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Weapon/RocketProjectile.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARocketProjectile_Statics::NewProp_RocketMovementComp = { "RocketMovementComp", nullptr, (EPropertyFlags)0x00200800000a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARocketProjectile, RocketMovementComp), Z_Construct_UClass_UProjectileMovementComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARocketProjectile_Statics::NewProp_RocketMovementComp_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARocketProjectile_Statics::NewProp_RocketMovementComp_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ARocketProjectile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARocketProjectile_Statics::NewProp_OnHitFireRocket,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARocketProjectile_Statics::NewProp_RocketBullet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARocketProjectile_Statics::NewProp_RocketMovementComp,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARocketProjectile_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARocketProjectile>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ARocketProjectile_Statics::ClassParams = {
		&ARocketProjectile::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ARocketProjectile_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_ARocketProjectile_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ARocketProjectile_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ARocketProjectile_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARocketProjectile()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ARocketProjectile_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ARocketProjectile, 1618230614);
	template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<ARocketProjectile>()
	{
		return ARocketProjectile::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ARocketProjectile(Z_Construct_UClass_ARocketProjectile, &ARocketProjectile::StaticClass, TEXT("/Script/TestProjectArtCraft"), TEXT("ARocketProjectile"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARocketProjectile);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
