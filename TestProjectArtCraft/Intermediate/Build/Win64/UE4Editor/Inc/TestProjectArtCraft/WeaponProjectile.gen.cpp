// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TestProjectArtCraft/Weapon/WeaponProjectile.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWeaponProjectile() {}
// Cross Module References
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_AWeaponProjectile_NoRegister();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_AWeaponProjectile();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_ABaseWeapon();
	UPackage* Z_Construct_UPackage__Script_TestProjectArtCraft();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_ABaseProjectile_NoRegister();
// End Cross Module References
	void AWeaponProjectile::StaticRegisterNativesAWeaponProjectile()
	{
	}
	UClass* Z_Construct_UClass_AWeaponProjectile_NoRegister()
	{
		return AWeaponProjectile::StaticClass();
	}
	struct Z_Construct_UClass_AWeaponProjectile_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectileClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ProjectileClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWeaponProjectile_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ABaseWeapon,
		(UObject* (*)())Z_Construct_UPackage__Script_TestProjectArtCraft,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponProjectile_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Weapon/WeaponProjectile.h" },
		{ "ModuleRelativePath", "Weapon/WeaponProjectile.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponProjectile_Statics::NewProp_ProjectileClass_MetaData[] = {
		{ "Category", "WeaponProjectile" },
		{ "ModuleRelativePath", "Weapon/WeaponProjectile.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AWeaponProjectile_Statics::NewProp_ProjectileClass = { "ProjectileClass", nullptr, (EPropertyFlags)0x0024080000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWeaponProjectile, ProjectileClass), Z_Construct_UClass_ABaseProjectile_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AWeaponProjectile_Statics::NewProp_ProjectileClass_MetaData, ARRAY_COUNT(Z_Construct_UClass_AWeaponProjectile_Statics::NewProp_ProjectileClass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWeaponProjectile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponProjectile_Statics::NewProp_ProjectileClass,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWeaponProjectile_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWeaponProjectile>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWeaponProjectile_Statics::ClassParams = {
		&AWeaponProjectile::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AWeaponProjectile_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_AWeaponProjectile_Statics::PropPointers),
		0,
		0x009000A1u,
		METADATA_PARAMS(Z_Construct_UClass_AWeaponProjectile_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AWeaponProjectile_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWeaponProjectile()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWeaponProjectile_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWeaponProjectile, 19637015);
	template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<AWeaponProjectile>()
	{
		return AWeaponProjectile::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWeaponProjectile(Z_Construct_UClass_AWeaponProjectile, &AWeaponProjectile::StaticClass, TEXT("/Script/TestProjectArtCraft"), TEXT("AWeaponProjectile"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWeaponProjectile);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
