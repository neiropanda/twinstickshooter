// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UDamageType;
class AController;
#ifdef TESTPROJECTARTCRAFT_VitalityComponent_generated_h
#error "VitalityComponent.generated.h already included, missing '#pragma once' in VitalityComponent.h"
#endif
#define TESTPROJECTARTCRAFT_VitalityComponent_generated_h

#define TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_9_DELEGATE \
static inline void FNoMyParamDelegate_DelegateWrapper(const FMulticastScriptDelegate& NoMyParamDelegate) \
{ \
	NoMyParamDelegate.ProcessMulticastDelegate<UObject>(NULL); \
}


#define TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execDamageHandle) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_DamagedActor); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Damage); \
		P_GET_OBJECT(UDamageType,Z_Param_DamageType); \
		P_GET_OBJECT(AController,Z_Param_InstigatedBy); \
		P_GET_OBJECT(AActor,Z_Param_DamageCauser); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DamageHandle(Z_Param_DamagedActor,Z_Param_Damage,Z_Param_DamageType,Z_Param_InstigatedBy,Z_Param_DamageCauser); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRegeneration) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Regeneration(); \
		P_NATIVE_END; \
	}


#define TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execDamageHandle) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_DamagedActor); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Damage); \
		P_GET_OBJECT(UDamageType,Z_Param_DamageType); \
		P_GET_OBJECT(AController,Z_Param_InstigatedBy); \
		P_GET_OBJECT(AActor,Z_Param_DamageCauser); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DamageHandle(Z_Param_DamagedActor,Z_Param_Damage,Z_Param_DamageType,Z_Param_InstigatedBy,Z_Param_DamageCauser); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRegeneration) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Regeneration(); \
		P_NATIVE_END; \
	}


#define TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVitalityComponent(); \
	friend struct Z_Construct_UClass_UVitalityComponent_Statics; \
public: \
	DECLARE_CLASS(UVitalityComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(UVitalityComponent)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUVitalityComponent(); \
	friend struct Z_Construct_UClass_UVitalityComponent_Statics; \
public: \
	DECLARE_CLASS(UVitalityComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(UVitalityComponent)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVitalityComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVitalityComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVitalityComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVitalityComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVitalityComponent(UVitalityComponent&&); \
	NO_API UVitalityComponent(const UVitalityComponent&); \
public:


#define TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVitalityComponent(UVitalityComponent&&); \
	NO_API UVitalityComponent(const UVitalityComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVitalityComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVitalityComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UVitalityComponent)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_14_PRIVATE_PROPERTY_OFFSET
#define TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_11_PROLOG
#define TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_14_RPC_WRAPPERS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_14_INCLASS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_14_INCLASS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<class UVitalityComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TestProjectArtCraft_Source_TestProjectArtCraft_Vitality_VitalityComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
