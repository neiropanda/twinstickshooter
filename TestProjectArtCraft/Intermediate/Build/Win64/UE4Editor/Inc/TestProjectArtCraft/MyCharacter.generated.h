// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TESTPROJECTARTCRAFT_MyCharacter_generated_h
#error "MyCharacter.generated.h already included, missing '#pragma once' in MyCharacter.h"
#endif
#define TESTPROJECTARTCRAFT_MyCharacter_generated_h

#define TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_10_DELEGATE \
struct _Script_TestProjectArtCraft_eventMyDelegate_Parms \
{ \
	bool bMyBool; \
}; \
static inline void FMyDelegate_DelegateWrapper(const FMulticastScriptDelegate& MyDelegate, bool bMyBool) \
{ \
	_Script_TestProjectArtCraft_eventMyDelegate_Parms Parms; \
	Parms.bMyBool=bMyBool ? true : false; \
	MyDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_19_RPC_WRAPPERS
#define TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyCharacter(); \
	friend struct Z_Construct_UClass_AMyCharacter_Statics; \
public: \
	DECLARE_CLASS(AMyCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(AMyCharacter)


#define TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_19_INCLASS \
private: \
	static void StaticRegisterNativesAMyCharacter(); \
	friend struct Z_Construct_UClass_AMyCharacter_Statics; \
public: \
	DECLARE_CLASS(AMyCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(AMyCharacter)


#define TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyCharacter(AMyCharacter&&); \
	NO_API AMyCharacter(const AMyCharacter&); \
public:


#define TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyCharacter(AMyCharacter&&); \
	NO_API AMyCharacter(const AMyCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyCharacter)


#define TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__VitalityComp() { return STRUCT_OFFSET(AMyCharacter, VitalityComp); } \
	FORCEINLINE static uint32 __PPO__CameraComponent() { return STRUCT_OFFSET(AMyCharacter, CameraComponent); } \
	FORCEINLINE static uint32 __PPO__SpringArmComponent() { return STRUCT_OFFSET(AMyCharacter, SpringArmComponent); } \
	FORCEINLINE static uint32 __PPO__CharacterMovementComponent() { return STRUCT_OFFSET(AMyCharacter, CharacterMovementComponent); } \
	FORCEINLINE static uint32 __PPO__bIsSprinting() { return STRUCT_OFFSET(AMyCharacter, bIsSprinting); } \
	FORCEINLINE static uint32 __PPO__bCanFire() { return STRUCT_OFFSET(AMyCharacter, bCanFire); } \
	FORCEINLINE static uint32 __PPO__MaxWalkSpeed() { return STRUCT_OFFSET(AMyCharacter, MaxWalkSpeed); } \
	FORCEINLINE static uint32 __PPO__DefaultWalkSpeed() { return STRUCT_OFFSET(AMyCharacter, DefaultWalkSpeed); } \
	FORCEINLINE static uint32 __PPO__CameraStep() { return STRUCT_OFFSET(AMyCharacter, CameraStep); } \
	FORCEINLINE static uint32 __PPO__MinCameraLenght() { return STRUCT_OFFSET(AMyCharacter, MinCameraLenght); } \
	FORCEINLINE static uint32 __PPO__MaxCameraLength() { return STRUCT_OFFSET(AMyCharacter, MaxCameraLength); }


#define TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_16_PROLOG
#define TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_19_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_19_RPC_WRAPPERS \
	TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_19_INCLASS \
	TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_19_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_19_INCLASS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<class AMyCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TestProjectArtCraft_Source_TestProjectArtCraft_MyCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
