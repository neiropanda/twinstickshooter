// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FHitResult;
#ifdef TESTPROJECTARTCRAFT_ImpactEffectComponent_generated_h
#error "ImpactEffectComponent.generated.h already included, missing '#pragma once' in ImpactEffectComponent.h"
#endif
#define TESTPROJECTARTCRAFT_ImpactEffectComponent_generated_h

#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpawnImpactEffect) \
	{ \
		P_GET_STRUCT(FHitResult,Z_Param_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SpawnImpactEffect(Z_Param_Hit); \
		P_NATIVE_END; \
	}


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpawnImpactEffect) \
	{ \
		P_GET_STRUCT(FHitResult,Z_Param_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SpawnImpactEffect(Z_Param_Hit); \
		P_NATIVE_END; \
	}


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUImpactEffectComponent(); \
	friend struct Z_Construct_UClass_UImpactEffectComponent_Statics; \
public: \
	DECLARE_CLASS(UImpactEffectComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(UImpactEffectComponent)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUImpactEffectComponent(); \
	friend struct Z_Construct_UClass_UImpactEffectComponent_Statics; \
public: \
	DECLARE_CLASS(UImpactEffectComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(UImpactEffectComponent)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UImpactEffectComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UImpactEffectComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UImpactEffectComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImpactEffectComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UImpactEffectComponent(UImpactEffectComponent&&); \
	NO_API UImpactEffectComponent(const UImpactEffectComponent&); \
public:


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UImpactEffectComponent(UImpactEffectComponent&&); \
	NO_API UImpactEffectComponent(const UImpactEffectComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UImpactEffectComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImpactEffectComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UImpactEffectComponent)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ImpactActor_Class() { return STRUCT_OFFSET(UImpactEffectComponent, ImpactActor_Class); }


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h_10_PROLOG
#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h_13_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h_13_RPC_WRAPPERS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h_13_INCLASS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h_13_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h_13_INCLASS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<class UImpactEffectComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
