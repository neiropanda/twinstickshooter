// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TESTPROJECTARTCRAFT_WeaponTrace_generated_h
#error "WeaponTrace.generated.h already included, missing '#pragma once' in WeaponTrace.h"
#endif
#define TESTPROJECTARTCRAFT_WeaponTrace_generated_h

#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h_15_RPC_WRAPPERS
#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWeaponTrace(); \
	friend struct Z_Construct_UClass_AWeaponTrace_Statics; \
public: \
	DECLARE_CLASS(AWeaponTrace, ABaseWeapon, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(AWeaponTrace)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAWeaponTrace(); \
	friend struct Z_Construct_UClass_AWeaponTrace_Statics; \
public: \
	DECLARE_CLASS(AWeaponTrace, ABaseWeapon, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(AWeaponTrace)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWeaponTrace(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWeaponTrace) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeaponTrace); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeaponTrace); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeaponTrace(AWeaponTrace&&); \
	NO_API AWeaponTrace(const AWeaponTrace&); \
public:


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWeaponTrace() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeaponTrace(AWeaponTrace&&); \
	NO_API AWeaponTrace(const AWeaponTrace&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeaponTrace); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeaponTrace); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWeaponTrace)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h_15_PRIVATE_PROPERTY_OFFSET
#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h_12_PROLOG
#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h_15_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h_15_RPC_WRAPPERS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h_15_INCLASS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h_15_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h_15_INCLASS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<class AWeaponTrace>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_WeaponTrace_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
