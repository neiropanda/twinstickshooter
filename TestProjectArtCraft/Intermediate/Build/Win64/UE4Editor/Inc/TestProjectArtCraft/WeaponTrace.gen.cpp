// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TestProjectArtCraft/Weapon/WeaponTrace.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWeaponTrace() {}
// Cross Module References
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_AWeaponTrace_NoRegister();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_AWeaponTrace();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_ABaseWeapon();
	UPackage* Z_Construct_UPackage__Script_TestProjectArtCraft();
// End Cross Module References
	void AWeaponTrace::StaticRegisterNativesAWeaponTrace()
	{
	}
	UClass* Z_Construct_UClass_AWeaponTrace_NoRegister()
	{
		return AWeaponTrace::StaticClass();
	}
	struct Z_Construct_UClass_AWeaponTrace_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SphereRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SphereRadius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TraceLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TraceLength;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWeaponTrace_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ABaseWeapon,
		(UObject* (*)())Z_Construct_UPackage__Script_TestProjectArtCraft,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponTrace_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Weapon/WeaponTrace.h" },
		{ "ModuleRelativePath", "Weapon/WeaponTrace.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponTrace_Statics::NewProp_SphereRadius_MetaData[] = {
		{ "Category", "Trace" },
		{ "ModuleRelativePath", "Weapon/WeaponTrace.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AWeaponTrace_Statics::NewProp_SphereRadius = { "SphereRadius", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWeaponTrace, SphereRadius), METADATA_PARAMS(Z_Construct_UClass_AWeaponTrace_Statics::NewProp_SphereRadius_MetaData, ARRAY_COUNT(Z_Construct_UClass_AWeaponTrace_Statics::NewProp_SphereRadius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponTrace_Statics::NewProp_TraceLength_MetaData[] = {
		{ "Category", "Trace" },
		{ "ModuleRelativePath", "Weapon/WeaponTrace.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AWeaponTrace_Statics::NewProp_TraceLength = { "TraceLength", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWeaponTrace, TraceLength), METADATA_PARAMS(Z_Construct_UClass_AWeaponTrace_Statics::NewProp_TraceLength_MetaData, ARRAY_COUNT(Z_Construct_UClass_AWeaponTrace_Statics::NewProp_TraceLength_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWeaponTrace_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponTrace_Statics::NewProp_SphereRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponTrace_Statics::NewProp_TraceLength,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWeaponTrace_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWeaponTrace>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWeaponTrace_Statics::ClassParams = {
		&AWeaponTrace::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AWeaponTrace_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_AWeaponTrace_Statics::PropPointers),
		0,
		0x009000A1u,
		METADATA_PARAMS(Z_Construct_UClass_AWeaponTrace_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AWeaponTrace_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWeaponTrace()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWeaponTrace_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWeaponTrace, 2568598981);
	template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<AWeaponTrace>()
	{
		return AWeaponTrace::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWeaponTrace(Z_Construct_UClass_AWeaponTrace, &AWeaponTrace::StaticClass, TEXT("/Script/TestProjectArtCraft"), TEXT("AWeaponTrace"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWeaponTrace);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
