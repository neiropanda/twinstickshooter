// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TestProjectArtCraft/Vitality/VitalityComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVitalityComponent() {}
// Cross Module References
	TESTPROJECTARTCRAFT_API UFunction* Z_Construct_UDelegateFunction_TestProjectArtCraft_NoMyParamDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_TestProjectArtCraft();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_UVitalityComponent_NoRegister();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_UVitalityComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	TESTPROJECTARTCRAFT_API UFunction* Z_Construct_UFunction_UVitalityComponent_DamageHandle();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AController_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UDamageType_NoRegister();
	TESTPROJECTARTCRAFT_API UFunction* Z_Construct_UFunction_UVitalityComponent_Regeneration();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_TestProjectArtCraft_NoMyParamDelegate__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_TestProjectArtCraft_NoMyParamDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Vitality/VitalityComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_TestProjectArtCraft_NoMyParamDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_TestProjectArtCraft, nullptr, "NoMyParamDelegate__DelegateSignature", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_TestProjectArtCraft_NoMyParamDelegate__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_TestProjectArtCraft_NoMyParamDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_TestProjectArtCraft_NoMyParamDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_TestProjectArtCraft_NoMyParamDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	void UVitalityComponent::StaticRegisterNativesUVitalityComponent()
	{
		UClass* Class = UVitalityComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "DamageHandle", &UVitalityComponent::execDamageHandle },
			{ "Regeneration", &UVitalityComponent::execRegeneration },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics
	{
		struct VitalityComponent_eventDamageHandle_Parms
		{
			AActor* DamagedActor;
			float Damage;
			const UDamageType* DamageType;
			AController* InstigatedBy;
			AActor* DamageCauser;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DamageCauser;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InstigatedBy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DamageType_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DamageType;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Damage;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DamagedActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::NewProp_DamageCauser = { "DamageCauser", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VitalityComponent_eventDamageHandle_Parms, DamageCauser), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::NewProp_InstigatedBy = { "InstigatedBy", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VitalityComponent_eventDamageHandle_Parms, InstigatedBy), Z_Construct_UClass_AController_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::NewProp_DamageType_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::NewProp_DamageType = { "DamageType", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VitalityComponent_eventDamageHandle_Parms, DamageType), Z_Construct_UClass_UDamageType_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::NewProp_DamageType_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::NewProp_DamageType_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::NewProp_Damage = { "Damage", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VitalityComponent_eventDamageHandle_Parms, Damage), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::NewProp_DamagedActor = { "DamagedActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VitalityComponent_eventDamageHandle_Parms, DamagedActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::NewProp_DamageCauser,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::NewProp_InstigatedBy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::NewProp_DamageType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::NewProp_Damage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::NewProp_DamagedActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Vitality/VitalityComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVitalityComponent, nullptr, "DamageHandle", sizeof(VitalityComponent_eventDamageHandle_Parms), Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVitalityComponent_DamageHandle()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVitalityComponent_DamageHandle_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVitalityComponent_Regeneration_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVitalityComponent_Regeneration_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Vitality/VitalityComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVitalityComponent_Regeneration_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVitalityComponent, nullptr, "Regeneration", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVitalityComponent_Regeneration_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVitalityComponent_Regeneration_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVitalityComponent_Regeneration()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVitalityComponent_Regeneration_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UVitalityComponent_NoRegister()
	{
		return UVitalityComponent::StaticClass();
	}
	struct Z_Construct_UClass_UVitalityComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnOwnerDied_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnOwnerDied;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnHeathChanged_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnHeathChanged;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RegenDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RegenDelay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RegenPerSec_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RegenPerSec;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentHealth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurrentHealth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxHealth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxHealth;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVitalityComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_TestProjectArtCraft,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UVitalityComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UVitalityComponent_DamageHandle, "DamageHandle" }, // 653776568
		{ &Z_Construct_UFunction_UVitalityComponent_Regeneration, "Regeneration" }, // 103204401
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVitalityComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "Vitality/VitalityComponent.h" },
		{ "ModuleRelativePath", "Vitality/VitalityComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVitalityComponent_Statics::NewProp_OnOwnerDied_MetaData[] = {
		{ "Category", "Delegates" },
		{ "ModuleRelativePath", "Vitality/VitalityComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UVitalityComponent_Statics::NewProp_OnOwnerDied = { "OnOwnerDied", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::MulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVitalityComponent, OnOwnerDied), Z_Construct_UDelegateFunction_TestProjectArtCraft_NoMyParamDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UVitalityComponent_Statics::NewProp_OnOwnerDied_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVitalityComponent_Statics::NewProp_OnOwnerDied_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVitalityComponent_Statics::NewProp_OnHeathChanged_MetaData[] = {
		{ "Category", "Delegates" },
		{ "ModuleRelativePath", "Vitality/VitalityComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UVitalityComponent_Statics::NewProp_OnHeathChanged = { "OnHeathChanged", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::MulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVitalityComponent, OnHeathChanged), Z_Construct_UDelegateFunction_TestProjectArtCraft_NoMyParamDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UVitalityComponent_Statics::NewProp_OnHeathChanged_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVitalityComponent_Statics::NewProp_OnHeathChanged_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVitalityComponent_Statics::NewProp_RegenDelay_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "Vitality/VitalityComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVitalityComponent_Statics::NewProp_RegenDelay = { "RegenDelay", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVitalityComponent, RegenDelay), METADATA_PARAMS(Z_Construct_UClass_UVitalityComponent_Statics::NewProp_RegenDelay_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVitalityComponent_Statics::NewProp_RegenDelay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVitalityComponent_Statics::NewProp_RegenPerSec_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "Vitality/VitalityComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVitalityComponent_Statics::NewProp_RegenPerSec = { "RegenPerSec", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVitalityComponent, RegenPerSec), METADATA_PARAMS(Z_Construct_UClass_UVitalityComponent_Statics::NewProp_RegenPerSec_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVitalityComponent_Statics::NewProp_RegenPerSec_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVitalityComponent_Statics::NewProp_CurrentHealth_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "Vitality/VitalityComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVitalityComponent_Statics::NewProp_CurrentHealth = { "CurrentHealth", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVitalityComponent, CurrentHealth), METADATA_PARAMS(Z_Construct_UClass_UVitalityComponent_Statics::NewProp_CurrentHealth_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVitalityComponent_Statics::NewProp_CurrentHealth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVitalityComponent_Statics::NewProp_MaxHealth_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "Vitality/VitalityComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVitalityComponent_Statics::NewProp_MaxHealth = { "MaxHealth", nullptr, (EPropertyFlags)0x0010000000000815, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVitalityComponent, MaxHealth), METADATA_PARAMS(Z_Construct_UClass_UVitalityComponent_Statics::NewProp_MaxHealth_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVitalityComponent_Statics::NewProp_MaxHealth_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVitalityComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVitalityComponent_Statics::NewProp_OnOwnerDied,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVitalityComponent_Statics::NewProp_OnHeathChanged,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVitalityComponent_Statics::NewProp_RegenDelay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVitalityComponent_Statics::NewProp_RegenPerSec,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVitalityComponent_Statics::NewProp_CurrentHealth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVitalityComponent_Statics::NewProp_MaxHealth,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVitalityComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVitalityComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVitalityComponent_Statics::ClassParams = {
		&UVitalityComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UVitalityComponent_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_UVitalityComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UVitalityComponent_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UVitalityComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVitalityComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVitalityComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVitalityComponent, 59681105);
	template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<UVitalityComponent>()
	{
		return UVitalityComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVitalityComponent(Z_Construct_UClass_UVitalityComponent, &UVitalityComponent::StaticClass, TEXT("/Script/TestProjectArtCraft"), TEXT("UVitalityComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVitalityComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
