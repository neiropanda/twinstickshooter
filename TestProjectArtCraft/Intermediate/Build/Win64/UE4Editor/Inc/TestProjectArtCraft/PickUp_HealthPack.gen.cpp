// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TestProjectArtCraft/PickUp/PickUp_HealthPack.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePickUp_HealthPack() {}
// Cross Module References
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_APickUp_HealthPack_NoRegister();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_APickUp_HealthPack();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_ABasePickUp();
	UPackage* Z_Construct_UPackage__Script_TestProjectArtCraft();
	TESTPROJECTARTCRAFT_API UFunction* Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void APickUp_HealthPack::StaticRegisterNativesAPickUp_HealthPack()
	{
		UClass* Class = APickUp_HealthPack::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnPickUpHealth", &APickUp_HealthPack::execOnPickUpHealth },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics
	{
		struct PickUp_HealthPack_eventOnPickUpHealth_Parms
		{
			UPrimitiveComponent* OverlappedComp;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComp;
			int32 OtherBodyIndex;
			bool bFromSweep;
			FHitResult SweepResult;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SweepResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SweepResult;
		static void NewProp_bFromSweep_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFromSweep;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OtherBodyIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OtherComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherComp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlappedComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedComp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_SweepResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_SweepResult = { "SweepResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickUp_HealthPack_eventOnPickUpHealth_Parms, SweepResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_SweepResult_MetaData, ARRAY_COUNT(Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_SweepResult_MetaData)) };
	void Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_bFromSweep_SetBit(void* Obj)
	{
		((PickUp_HealthPack_eventOnPickUpHealth_Parms*)Obj)->bFromSweep = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_bFromSweep = { "bFromSweep", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PickUp_HealthPack_eventOnPickUpHealth_Parms), &Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_bFromSweep_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_OtherBodyIndex = { "OtherBodyIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickUp_HealthPack_eventOnPickUpHealth_Parms, OtherBodyIndex), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_OtherComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_OtherComp = { "OtherComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickUp_HealthPack_eventOnPickUpHealth_Parms, OtherComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_OtherComp_MetaData, ARRAY_COUNT(Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_OtherComp_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickUp_HealthPack_eventOnPickUpHealth_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_OverlappedComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_OverlappedComp = { "OverlappedComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickUp_HealthPack_eventOnPickUpHealth_Parms, OverlappedComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_OverlappedComp_MetaData, ARRAY_COUNT(Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_OverlappedComp_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_SweepResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_bFromSweep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_OtherBodyIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_OtherComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_OtherActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::NewProp_OverlappedComp,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PickUp/PickUp_HealthPack.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APickUp_HealthPack, nullptr, "OnPickUpHealth", sizeof(PickUp_HealthPack_eventOnPickUpHealth_Parms), Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APickUp_HealthPack_NoRegister()
	{
		return APickUp_HealthPack::StaticClass();
	}
	struct Z_Construct_UClass_APickUp_HealthPack_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HealthPoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HealthPoints;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APickUp_HealthPack_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ABasePickUp,
		(UObject* (*)())Z_Construct_UPackage__Script_TestProjectArtCraft,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APickUp_HealthPack_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APickUp_HealthPack_OnPickUpHealth, "OnPickUpHealth" }, // 958243744
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APickUp_HealthPack_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PickUp/PickUp_HealthPack.h" },
		{ "ModuleRelativePath", "PickUp/PickUp_HealthPack.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APickUp_HealthPack_Statics::NewProp_HealthPoints_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "PickUp/PickUp_HealthPack.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APickUp_HealthPack_Statics::NewProp_HealthPoints = { "HealthPoints", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APickUp_HealthPack, HealthPoints), METADATA_PARAMS(Z_Construct_UClass_APickUp_HealthPack_Statics::NewProp_HealthPoints_MetaData, ARRAY_COUNT(Z_Construct_UClass_APickUp_HealthPack_Statics::NewProp_HealthPoints_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APickUp_HealthPack_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APickUp_HealthPack_Statics::NewProp_HealthPoints,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APickUp_HealthPack_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APickUp_HealthPack>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APickUp_HealthPack_Statics::ClassParams = {
		&APickUp_HealthPack::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APickUp_HealthPack_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_APickUp_HealthPack_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APickUp_HealthPack_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APickUp_HealthPack_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APickUp_HealthPack()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APickUp_HealthPack_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APickUp_HealthPack, 786826664);
	template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<APickUp_HealthPack>()
	{
		return APickUp_HealthPack::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APickUp_HealthPack(Z_Construct_UClass_APickUp_HealthPack, &APickUp_HealthPack::StaticClass, TEXT("/Script/TestProjectArtCraft"), TEXT("APickUp_HealthPack"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APickUp_HealthPack);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
