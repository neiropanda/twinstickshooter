// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TESTPROJECTARTCRAFT_ImpactEffectActor_generated_h
#error "ImpactEffectActor.generated.h already included, missing '#pragma once' in ImpactEffectActor.h"
#endif
#define TESTPROJECTARTCRAFT_ImpactEffectActor_generated_h

#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h_12_RPC_WRAPPERS
#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAImpactEffectActor(); \
	friend struct Z_Construct_UClass_AImpactEffectActor_Statics; \
public: \
	DECLARE_CLASS(AImpactEffectActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(AImpactEffectActor)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAImpactEffectActor(); \
	friend struct Z_Construct_UClass_AImpactEffectActor_Statics; \
public: \
	DECLARE_CLASS(AImpactEffectActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(AImpactEffectActor)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AImpactEffectActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AImpactEffectActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AImpactEffectActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AImpactEffectActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AImpactEffectActor(AImpactEffectActor&&); \
	NO_API AImpactEffectActor(const AImpactEffectActor&); \
public:


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AImpactEffectActor(AImpactEffectActor&&); \
	NO_API AImpactEffectActor(const AImpactEffectActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AImpactEffectActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AImpactEffectActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AImpactEffectActor)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DecalMaterial() { return STRUCT_OFFSET(AImpactEffectActor, DecalMaterial); } \
	FORCEINLINE static uint32 __PPO__BulletSound() { return STRUCT_OFFSET(AImpactEffectActor, BulletSound); } \
	FORCEINLINE static uint32 __PPO__EffectParticle() { return STRUCT_OFFSET(AImpactEffectActor, EffectParticle); } \
	FORCEINLINE static uint32 __PPO__bApplyImpulse() { return STRUCT_OFFSET(AImpactEffectActor, bApplyImpulse); } \
	FORCEINLINE static uint32 __PPO__ImpulseStrength() { return STRUCT_OFFSET(AImpactEffectActor, ImpulseStrength); } \
	FORCEINLINE static uint32 __PPO__Damage() { return STRUCT_OFFSET(AImpactEffectActor, Damage); } \
	FORCEINLINE static uint32 __PPO__EffectHit() { return STRUCT_OFFSET(AImpactEffectActor, EffectHit); }


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h_9_PROLOG
#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h_12_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h_12_RPC_WRAPPERS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h_12_INCLASS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h_12_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h_12_INCLASS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<class AImpactEffectActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_ImpactEffectActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
