// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef TESTPROJECTARTCRAFT_PickUpWeapon_generated_h
#error "PickUpWeapon.generated.h already included, missing '#pragma once' in PickUpWeapon.h"
#endif
#define TESTPROJECTARTCRAFT_PickUpWeapon_generated_h

#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnPickUpWeapon) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnPickUpWeapon(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnPickUpWeapon) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnPickUpWeapon(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPickUpWeapon(); \
	friend struct Z_Construct_UClass_APickUpWeapon_Statics; \
public: \
	DECLARE_CLASS(APickUpWeapon, ABasePickUp, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(APickUpWeapon)


#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPickUpWeapon(); \
	friend struct Z_Construct_UClass_APickUpWeapon_Statics; \
public: \
	DECLARE_CLASS(APickUpWeapon, ABasePickUp, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(APickUpWeapon)


#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APickUpWeapon(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APickUpWeapon) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APickUpWeapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APickUpWeapon); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APickUpWeapon(APickUpWeapon&&); \
	NO_API APickUpWeapon(const APickUpWeapon&); \
public:


#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APickUpWeapon() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APickUpWeapon(APickUpWeapon&&); \
	NO_API APickUpWeapon(const APickUpWeapon&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APickUpWeapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APickUpWeapon); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APickUpWeapon)


#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PickUpWeapon_Class() { return STRUCT_OFFSET(APickUpWeapon, PickUpWeapon_Class); }


#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h_12_PROLOG
#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h_15_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h_15_RPC_WRAPPERS \
	TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h_15_INCLASS \
	TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h_15_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h_15_INCLASS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<class APickUpWeapon>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TestProjectArtCraft_Source_TestProjectArtCraft_PickUp_PickUpWeapon_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
