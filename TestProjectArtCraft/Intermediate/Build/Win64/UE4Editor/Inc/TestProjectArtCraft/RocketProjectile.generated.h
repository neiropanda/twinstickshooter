// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FHitResult;
#ifdef TESTPROJECTARTCRAFT_RocketProjectile_generated_h
#error "RocketProjectile.generated.h already included, missing '#pragma once' in RocketProjectile.h"
#endif
#define TESTPROJECTARTCRAFT_RocketProjectile_generated_h

#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_10_DELEGATE \
struct _Script_TestProjectArtCraft_eventHitResultDelegate2_Parms \
{ \
	FHitResult RocketHit; \
}; \
static inline void FHitResultDelegate2_DelegateWrapper(const FScriptDelegate& HitResultDelegate2, FHitResult RocketHit) \
{ \
	_Script_TestProjectArtCraft_eventHitResultDelegate2_Parms Parms; \
	Parms.RocketHit=RocketHit; \
	HitResultDelegate2.ProcessDelegate<UObject>(&Parms); \
}


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_15_RPC_WRAPPERS
#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARocketProjectile(); \
	friend struct Z_Construct_UClass_ARocketProjectile_Statics; \
public: \
	DECLARE_CLASS(ARocketProjectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(ARocketProjectile)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_15_INCLASS \
private: \
	static void StaticRegisterNativesARocketProjectile(); \
	friend struct Z_Construct_UClass_ARocketProjectile_Statics; \
public: \
	DECLARE_CLASS(ARocketProjectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(ARocketProjectile)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARocketProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARocketProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARocketProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARocketProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARocketProjectile(ARocketProjectile&&); \
	NO_API ARocketProjectile(const ARocketProjectile&); \
public:


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARocketProjectile(ARocketProjectile&&); \
	NO_API ARocketProjectile(const ARocketProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARocketProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARocketProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARocketProjectile)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RocketMovementComp() { return STRUCT_OFFSET(ARocketProjectile, RocketMovementComp); } \
	FORCEINLINE static uint32 __PPO__RocketBullet() { return STRUCT_OFFSET(ARocketProjectile, RocketBullet); }


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_12_PROLOG
#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_15_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_15_RPC_WRAPPERS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_15_INCLASS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_15_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_15_INCLASS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<class ARocketProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_RocketProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
