// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TestProjectArtCraft/Weapon/ImpactEffectComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeImpactEffectComponent() {}
// Cross Module References
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_UImpactEffectComponent_NoRegister();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_UImpactEffectComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_TestProjectArtCraft();
	TESTPROJECTARTCRAFT_API UFunction* Z_Construct_UFunction_UImpactEffectComponent_SpawnImpactEffect();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_AImpactEffectActor_NoRegister();
// End Cross Module References
	void UImpactEffectComponent::StaticRegisterNativesUImpactEffectComponent()
	{
		UClass* Class = UImpactEffectComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SpawnImpactEffect", &UImpactEffectComponent::execSpawnImpactEffect },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UImpactEffectComponent_SpawnImpactEffect_Statics
	{
		struct ImpactEffectComponent_eventSpawnImpactEffect_Parms
		{
			FHitResult Hit;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Hit;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UImpactEffectComponent_SpawnImpactEffect_Statics::NewProp_Hit = { "Hit", nullptr, (EPropertyFlags)0x0010008000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ImpactEffectComponent_eventSpawnImpactEffect_Parms, Hit), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UImpactEffectComponent_SpawnImpactEffect_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UImpactEffectComponent_SpawnImpactEffect_Statics::NewProp_Hit,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UImpactEffectComponent_SpawnImpactEffect_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Weapon/ImpactEffectComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UImpactEffectComponent_SpawnImpactEffect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UImpactEffectComponent, nullptr, "SpawnImpactEffect", sizeof(ImpactEffectComponent_eventSpawnImpactEffect_Parms), Z_Construct_UFunction_UImpactEffectComponent_SpawnImpactEffect_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UImpactEffectComponent_SpawnImpactEffect_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UImpactEffectComponent_SpawnImpactEffect_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UImpactEffectComponent_SpawnImpactEffect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UImpactEffectComponent_SpawnImpactEffect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UImpactEffectComponent_SpawnImpactEffect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UImpactEffectComponent_NoRegister()
	{
		return UImpactEffectComponent::StaticClass();
	}
	struct Z_Construct_UClass_UImpactEffectComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImpactActor_Class_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ImpactActor_Class;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UImpactEffectComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_TestProjectArtCraft,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UImpactEffectComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UImpactEffectComponent_SpawnImpactEffect, "SpawnImpactEffect" }, // 1988389483
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImpactEffectComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "Weapon/ImpactEffectComponent.h" },
		{ "ModuleRelativePath", "Weapon/ImpactEffectComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImpactEffectComponent_Statics::NewProp_ImpactActor_Class_MetaData[] = {
		{ "Category", "Effects" },
		{ "ModuleRelativePath", "Weapon/ImpactEffectComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UImpactEffectComponent_Statics::NewProp_ImpactActor_Class = { "ImpactActor_Class", nullptr, (EPropertyFlags)0x0024080000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UImpactEffectComponent, ImpactActor_Class), Z_Construct_UClass_AImpactEffectActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UImpactEffectComponent_Statics::NewProp_ImpactActor_Class_MetaData, ARRAY_COUNT(Z_Construct_UClass_UImpactEffectComponent_Statics::NewProp_ImpactActor_Class_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UImpactEffectComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UImpactEffectComponent_Statics::NewProp_ImpactActor_Class,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UImpactEffectComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UImpactEffectComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UImpactEffectComponent_Statics::ClassParams = {
		&UImpactEffectComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UImpactEffectComponent_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_UImpactEffectComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UImpactEffectComponent_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UImpactEffectComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UImpactEffectComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UImpactEffectComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UImpactEffectComponent, 1279559697);
	template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<UImpactEffectComponent>()
	{
		return UImpactEffectComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UImpactEffectComponent(Z_Construct_UClass_UImpactEffectComponent, &UImpactEffectComponent::StaticClass, TEXT("/Script/TestProjectArtCraft"), TEXT("UImpactEffectComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UImpactEffectComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
