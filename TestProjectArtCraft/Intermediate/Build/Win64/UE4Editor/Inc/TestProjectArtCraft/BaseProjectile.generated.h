// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FHitResult;
#ifdef TESTPROJECTARTCRAFT_BaseProjectile_generated_h
#error "BaseProjectile.generated.h already included, missing '#pragma once' in BaseProjectile.h"
#endif
#define TESTPROJECTARTCRAFT_BaseProjectile_generated_h

#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_9_DELEGATE \
struct _Script_TestProjectArtCraft_eventHitResultDelegate_Parms \
{ \
	FHitResult Hit; \
}; \
static inline void FHitResultDelegate_DelegateWrapper(const FScriptDelegate& HitResultDelegate, FHitResult Hit) \
{ \
	_Script_TestProjectArtCraft_eventHitResultDelegate_Parms Parms; \
	Parms.Hit=Hit; \
	HitResultDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_14_RPC_WRAPPERS
#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABaseProjectile(); \
	friend struct Z_Construct_UClass_ABaseProjectile_Statics; \
public: \
	DECLARE_CLASS(ABaseProjectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(ABaseProjectile)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_14_INCLASS \
private: \
	static void StaticRegisterNativesABaseProjectile(); \
	friend struct Z_Construct_UClass_ABaseProjectile_Statics; \
public: \
	DECLARE_CLASS(ABaseProjectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TestProjectArtCraft"), NO_API) \
	DECLARE_SERIALIZER(ABaseProjectile)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABaseProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABaseProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABaseProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABaseProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABaseProjectile(ABaseProjectile&&); \
	NO_API ABaseProjectile(const ABaseProjectile&); \
public:


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABaseProjectile(ABaseProjectile&&); \
	NO_API ABaseProjectile(const ABaseProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABaseProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABaseProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABaseProjectile)


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MovementComp() { return STRUCT_OFFSET(ABaseProjectile, MovementComp); } \
	FORCEINLINE static uint32 __PPO__Bullet() { return STRUCT_OFFSET(ABaseProjectile, Bullet); }


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_11_PROLOG
#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_14_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_14_RPC_WRAPPERS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_14_INCLASS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_14_PRIVATE_PROPERTY_OFFSET \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_14_INCLASS_NO_PURE_DECLS \
	TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<class ABaseProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TestProjectArtCraft_Source_TestProjectArtCraft_Weapon_BaseProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
