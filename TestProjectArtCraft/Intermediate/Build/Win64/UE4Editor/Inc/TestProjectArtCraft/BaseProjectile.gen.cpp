// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TestProjectArtCraft/Weapon/BaseProjectile.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBaseProjectile() {}
// Cross Module References
	TESTPROJECTARTCRAFT_API UFunction* Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_TestProjectArtCraft();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_ABaseProjectile_NoRegister();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_ABaseProjectile();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UProjectileMovementComponent_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate__DelegateSignature_Statics
	{
		struct _Script_TestProjectArtCraft_eventHitResultDelegate_Parms
		{
			FHitResult Hit;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Hit;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate__DelegateSignature_Statics::NewProp_Hit = { "Hit", nullptr, (EPropertyFlags)0x0010008000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_TestProjectArtCraft_eventHitResultDelegate_Parms, Hit), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate__DelegateSignature_Statics::NewProp_Hit,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Weapon/BaseProjectile.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_TestProjectArtCraft, nullptr, "HitResultDelegate__DelegateSignature", sizeof(_Script_TestProjectArtCraft_eventHitResultDelegate_Parms), Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate__DelegateSignature_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	void ABaseProjectile::StaticRegisterNativesABaseProjectile()
	{
	}
	UClass* Z_Construct_UClass_ABaseProjectile_NoRegister()
	{
		return ABaseProjectile::StaticClass();
	}
	struct Z_Construct_UClass_ABaseProjectile_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnHitFire_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_OnHitFire;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bullet_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Bullet;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MovementComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MovementComp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABaseProjectile_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_TestProjectArtCraft,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABaseProjectile_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Weapon/BaseProjectile.h" },
		{ "ModuleRelativePath", "Weapon/BaseProjectile.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABaseProjectile_Statics::NewProp_OnHitFire_MetaData[] = {
		{ "ModuleRelativePath", "Weapon/BaseProjectile.h" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UClass_ABaseProjectile_Statics::NewProp_OnHitFire = { "OnHitFire", nullptr, (EPropertyFlags)0x0010000000080000, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABaseProjectile, OnHitFire), Z_Construct_UDelegateFunction_TestProjectArtCraft_HitResultDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_ABaseProjectile_Statics::NewProp_OnHitFire_MetaData, ARRAY_COUNT(Z_Construct_UClass_ABaseProjectile_Statics::NewProp_OnHitFire_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABaseProjectile_Statics::NewProp_Bullet_MetaData[] = {
		{ "Category", "BaseProjectile" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Weapon/BaseProjectile.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABaseProjectile_Statics::NewProp_Bullet = { "Bullet", nullptr, (EPropertyFlags)0x00200800000a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABaseProjectile, Bullet), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABaseProjectile_Statics::NewProp_Bullet_MetaData, ARRAY_COUNT(Z_Construct_UClass_ABaseProjectile_Statics::NewProp_Bullet_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABaseProjectile_Statics::NewProp_MovementComp_MetaData[] = {
		{ "Category", "BaseProjectile" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Weapon/BaseProjectile.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABaseProjectile_Statics::NewProp_MovementComp = { "MovementComp", nullptr, (EPropertyFlags)0x00200800000a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABaseProjectile, MovementComp), Z_Construct_UClass_UProjectileMovementComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABaseProjectile_Statics::NewProp_MovementComp_MetaData, ARRAY_COUNT(Z_Construct_UClass_ABaseProjectile_Statics::NewProp_MovementComp_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABaseProjectile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABaseProjectile_Statics::NewProp_OnHitFire,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABaseProjectile_Statics::NewProp_Bullet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABaseProjectile_Statics::NewProp_MovementComp,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABaseProjectile_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABaseProjectile>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABaseProjectile_Statics::ClassParams = {
		&ABaseProjectile::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ABaseProjectile_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_ABaseProjectile_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ABaseProjectile_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ABaseProjectile_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABaseProjectile()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABaseProjectile_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABaseProjectile, 3481699308);
	template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<ABaseProjectile>()
	{
		return ABaseProjectile::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABaseProjectile(Z_Construct_UClass_ABaseProjectile, &ABaseProjectile::StaticClass, TEXT("/Script/TestProjectArtCraft"), TEXT("ABaseProjectile"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABaseProjectile);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
