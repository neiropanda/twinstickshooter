// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TestProjectArtCraft/PickUp/PickUpWeapon.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePickUpWeapon() {}
// Cross Module References
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_APickUpWeapon_NoRegister();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_APickUpWeapon();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_ABasePickUp();
	UPackage* Z_Construct_UPackage__Script_TestProjectArtCraft();
	TESTPROJECTARTCRAFT_API UFunction* Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	TESTPROJECTARTCRAFT_API UClass* Z_Construct_UClass_ABaseWeapon_NoRegister();
// End Cross Module References
	void APickUpWeapon::StaticRegisterNativesAPickUpWeapon()
	{
		UClass* Class = APickUpWeapon::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnPickUpWeapon", &APickUpWeapon::execOnPickUpWeapon },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics
	{
		struct PickUpWeapon_eventOnPickUpWeapon_Parms
		{
			UPrimitiveComponent* OverlappedComp;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComp;
			int32 OtherBodyIndex;
			bool bFromSweep;
			FHitResult SweepResult;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SweepResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SweepResult;
		static void NewProp_bFromSweep_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFromSweep;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OtherBodyIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OtherComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherComp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlappedComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedComp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_SweepResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_SweepResult = { "SweepResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickUpWeapon_eventOnPickUpWeapon_Parms, SweepResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_SweepResult_MetaData, ARRAY_COUNT(Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_SweepResult_MetaData)) };
	void Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_bFromSweep_SetBit(void* Obj)
	{
		((PickUpWeapon_eventOnPickUpWeapon_Parms*)Obj)->bFromSweep = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_bFromSweep = { "bFromSweep", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PickUpWeapon_eventOnPickUpWeapon_Parms), &Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_bFromSweep_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_OtherBodyIndex = { "OtherBodyIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickUpWeapon_eventOnPickUpWeapon_Parms, OtherBodyIndex), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_OtherComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_OtherComp = { "OtherComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickUpWeapon_eventOnPickUpWeapon_Parms, OtherComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_OtherComp_MetaData, ARRAY_COUNT(Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_OtherComp_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickUpWeapon_eventOnPickUpWeapon_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_OverlappedComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_OverlappedComp = { "OverlappedComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickUpWeapon_eventOnPickUpWeapon_Parms, OverlappedComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_OverlappedComp_MetaData, ARRAY_COUNT(Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_OverlappedComp_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_SweepResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_bFromSweep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_OtherBodyIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_OtherComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_OtherActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::NewProp_OverlappedComp,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PickUp/PickUpWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APickUpWeapon, nullptr, "OnPickUpWeapon", sizeof(PickUpWeapon_eventOnPickUpWeapon_Parms), Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00480401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APickUpWeapon_NoRegister()
	{
		return APickUpWeapon::StaticClass();
	}
	struct Z_Construct_UClass_APickUpWeapon_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PickUpWeapon_Class_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_PickUpWeapon_Class;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APickUpWeapon_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ABasePickUp,
		(UObject* (*)())Z_Construct_UPackage__Script_TestProjectArtCraft,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APickUpWeapon_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APickUpWeapon_OnPickUpWeapon, "OnPickUpWeapon" }, // 2111659739
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APickUpWeapon_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PickUp/PickUpWeapon.h" },
		{ "ModuleRelativePath", "PickUp/PickUpWeapon.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APickUpWeapon_Statics::NewProp_PickUpWeapon_Class_MetaData[] = {
		{ "Category", "PickUp" },
		{ "ModuleRelativePath", "PickUp/PickUpWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_APickUpWeapon_Statics::NewProp_PickUpWeapon_Class = { "PickUpWeapon_Class", nullptr, (EPropertyFlags)0x0024080000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APickUpWeapon, PickUpWeapon_Class), Z_Construct_UClass_ABaseWeapon_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_APickUpWeapon_Statics::NewProp_PickUpWeapon_Class_MetaData, ARRAY_COUNT(Z_Construct_UClass_APickUpWeapon_Statics::NewProp_PickUpWeapon_Class_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APickUpWeapon_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APickUpWeapon_Statics::NewProp_PickUpWeapon_Class,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APickUpWeapon_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APickUpWeapon>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APickUpWeapon_Statics::ClassParams = {
		&APickUpWeapon::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APickUpWeapon_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_APickUpWeapon_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APickUpWeapon_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APickUpWeapon_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APickUpWeapon()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APickUpWeapon_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APickUpWeapon, 2549763360);
	template<> TESTPROJECTARTCRAFT_API UClass* StaticClass<APickUpWeapon>()
	{
		return APickUpWeapon::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APickUpWeapon(Z_Construct_UClass_APickUpWeapon, &APickUpWeapon::StaticClass, TEXT("/Script/TestProjectArtCraft"), TEXT("APickUpWeapon"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APickUpWeapon);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
