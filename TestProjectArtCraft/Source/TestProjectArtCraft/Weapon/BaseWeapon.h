// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/SkeletalMeshActor.h"
#include "BaseWeapon.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamDelegate);

/**
 * 
 */
UCLASS(abstract)
class TESTPROJECTARTCRAFT_API ABaseWeapon : public ASkeletalMeshActor
{
	GENERATED_BODY()

public:

	ABaseWeapon();

	virtual void StartFire();
	virtual void StopFire();

	void Reload();
	virtual void Fire();

	UFUNCTION()
		void PlayReloadMontage();
	UFUNCTION()
		void PlayReloadSound();
	UFUNCTION()
		void PlayFireSound();
	

protected:


	UPROPERTY()
	bool bIsSprinting = false;


	UPROPERTY(EditDefaultsOnly, Category = "Muzzle")
	FName MuzzleSocketName;


	//Shoots per minute
	UPROPERTY(EditDefaultsOnly, Category = "AUto Fire")
		float FireRate = 300;

	UPROPERTY(EditDefaultsOnly, Category = "AUto Fire")
		bool bAutoFire = false;

	UPROPERTY()
	class ACharacter* WeaponOwner;

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
	class USoundBase* ReloadSound;

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
	class USoundBase* FireSound;

	UPROPERTY(VisibleAnywhere)
	class UImpactEffectComponent* ImpactComp;

	FTimerHandle AutoFire_TH;

public:
	UPROPERTY(EditDefaultsOnly, Category = "Montages")
	class UAnimMontage* FireMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Montages")
	class UAnimMontage* ReloadMontage;

	UPROPERTY(BlueprintAssignable, Category = "Ammo")
		FNoParamDelegate OnAmmoChanged;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ammo")
		int32 AmmoPool;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ammo")
		int32 LoadedAmmo;

	//UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ammo")
		//int32 ClipSize;

	//UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ammo")
		//int32 MaxAmmo;


	FTransform GetMuzzleTransform();
	
	virtual void BeginPlay() override;
};
