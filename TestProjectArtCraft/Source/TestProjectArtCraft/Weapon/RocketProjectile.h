// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "RocketProjectile.generated.h"

DECLARE_DYNAMIC_DELEGATE_OneParam(FHitResultDelegate2, FHitResult, RocketHit);

UCLASS()
class TESTPROJECTARTCRAFT_API ARocketProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARocketProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
		class UProjectileMovementComponent* RocketMovementComp;

	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* RocketBullet;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void NotifyHitByRocket(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY()
	FHitResultDelegate2 OnHitFireRocket;

};
