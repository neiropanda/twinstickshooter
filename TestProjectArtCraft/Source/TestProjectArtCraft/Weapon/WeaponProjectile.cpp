// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponProjectile.h"
#include "BaseProjectile.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "ImpactEffectComponent.h"

void AWeaponProjectile::Fire()
{

	Super::Fire();

	if (ProjectileClass)
	{
		FTransform TmpTransform = GetMuzzleTransform();
		ABaseProjectile* TmpProject = Cast<ABaseProjectile>(GetWorld()->SpawnActor(ProjectileClass, &TmpTransform));


		TmpProject->OnHitFire.BindDynamic(ImpactComp, &UImpactEffectComponent::SpawnImpactEffect);
	}

	
}

