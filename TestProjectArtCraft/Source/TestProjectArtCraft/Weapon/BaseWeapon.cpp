// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseWeapon.h"
#include "MessageDialog.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "MyCharacter.h"
#include "Engine/Engine.h"
#include "ImpactEffectComponent.h"
#include "TimerManager.h"


ABaseWeapon::ABaseWeapon()
{
	GetSkeletalMeshComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetSkeletalMeshComponent()->SetMobility(EComponentMobility::Movable);


	ImpactComp = CreateDefaultSubobject<UImpactEffectComponent>(TEXT("ImpactEffectComponent"));

}

void ABaseWeapon::StartFire()
{
	//Fire();

	GetWorldTimerManager().SetTimer(AutoFire_TH, this, &ABaseWeapon::Fire, 60 / FireRate, bAutoFire, 0);

}

void ABaseWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(AutoFire_TH);
}

void ABaseWeapon::PlayReloadMontage()
{
	if (ReloadMontage)

	{
		if (WeaponOwner)
		{
			if (!WeaponOwner->GetMesh()->GetAnimInstance()->Montage_IsPlaying(ReloadMontage))
			{
				WeaponOwner->PlayAnimMontage(ReloadMontage);
				PlayReloadSound();
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("No Owner Found"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No Reload Montage Found"));
	}
}

void ABaseWeapon::PlayReloadSound()
{
	if (ReloadSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, ReloadSound, WeaponOwner->GetActorLocation());
	}
}


void ABaseWeapon::Reload()
{

	if (AmmoPool <= 0 || LoadedAmmo >= 10)
	{
		GEngine->AddOnScreenDebugMessage(0, 2.0f, FColor::Red, (TEXT("Press R to reload")));
		return;
	}

	if (AmmoPool < (10 - LoadedAmmo))
	{
		LoadedAmmo += AmmoPool;
	}
	else
	{
		AmmoPool = AmmoPool - (10 - LoadedAmmo);
		LoadedAmmo = 10;
	}
	
	/*if (TotalAmmo + CurrentClip >= ClipSize || TotalAmmo == 0)
	{
		TotalAmmo += CurrentClip - ClipSize;
		CurrentClip = ClipSize;
	}
	else
	{
		CurrentClip += TotalAmmo;
		TotalAmmo = 0;
	}*/

	WeaponOwner->PlayAnimMontage(ReloadMontage);
	PlayReloadSound();
	OnAmmoChanged.Broadcast();

}


void ABaseWeapon::Fire()
{

	if (LoadedAmmo > 0)
	{
		if (!WeaponOwner->GetMesh()->GetAnimInstance()->Montage_IsPlaying(ReloadMontage) && !bIsSprinting)
		{
			LoadedAmmo--;

			WeaponOwner->PlayAnimMontage(FireMontage);
			PlayFireSound();
			OnAmmoChanged.Broadcast();

			GEngine->AddOnScreenDebugMessage(0, 2.0f, FColor::Red, FString::FromInt(LoadedAmmo));
		}
	}
	//else
		//{
		//	Reload();
		//}

}

void ABaseWeapon::PlayFireSound()
{
	UGameplayStatics::PlaySoundAtLocation(this, FireSound, WeaponOwner->GetActorLocation());
}

FTransform ABaseWeapon::GetMuzzleTransform()
{
	if (MuzzleSocketName.IsNone())

	{
		FMessageDialog::Debugf(NSLOCTEXT("Error", "Key", "MuzzleSocketName not initialized"));
		return FTransform::Identity;
	}

	return GetSkeletalMeshComponent()->GetSocketTransform(MuzzleSocketName);

}

void ABaseWeapon::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwner())
	{
		WeaponOwner = Cast<ACharacter>(GetOwner());
		if (WeaponOwner)
		{
			UE_LOG(LogTemp, Warning, TEXT("Owner is not character"));
		}
	}


}