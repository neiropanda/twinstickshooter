// Fill out your copyright notice in the Description page of Project Settings.


#include "RocketProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/StaticMeshComponent.h"
#include "ImpactEffectActor.h"

// Sets default values
ARocketProjectile::ARocketProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RocketMovementComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("RocketMovementComponent"));
	RocketBullet = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RocketBullet"));
	RootComponent = RocketBullet;

}

// Called when the game starts or when spawned
void ARocketProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARocketProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SetLifeSpan(5);
}

void ARocketProjectile::NotifyHitByRocket(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	//Super::NotifyHitByRocket(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);

	if (OtherComp)
	{
		OtherComp->AddImpulse(HitLocation, "None", true);

		if (OnHitFireRocket.IsBound())
		{
			OnHitFireRocket.ExecuteIfBound(Hit);
		}
	}

	Destroy();

}

