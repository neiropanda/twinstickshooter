// Fill out your copyright notice in the Description page of Project Settings.


#include "RocketWeapon.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "ImpactEffectComponent.h"
#include "RocketProjectile.h"

void ARocketWeapon::Fire()
{
	Super::Fire();
	if (!bCanFire)
	{
	
	if (ProjectileClass)
	{
		FTransform TmpTransform = GetMuzzleTransform();
		ARocketProjectile* TmpProject = Cast<ARocketProjectile>(GetWorld()->SpawnActor(ProjectileClass, &TmpTransform));

		TmpProject->OnHitFireRocket.BindDynamic(ImpactComp, &UImpactEffectComponent::SpawnImpactEffect);
	}
	}
}