// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/BaseWeapon.h"
#include "RocketWeapon.generated.h"

/**
 * 
 */
UCLASS()
class TESTPROJECTARTCRAFT_API ARocketWeapon : public ABaseWeapon
{
	GENERATED_BODY()

protected:

	virtual void Fire();

		UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class ARocketProjectile> ProjectileClass;

		UPROPERTY(EditDefaultsOnly)
		bool bCanFire = false;
	
};
