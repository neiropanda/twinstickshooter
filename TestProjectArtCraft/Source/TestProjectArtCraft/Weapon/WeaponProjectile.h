// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/BaseWeapon.h"
#include "WeaponProjectile.generated.h"

/**
 * 
 */
UCLASS(abstract)
class TESTPROJECTARTCRAFT_API AWeaponProjectile : public ABaseWeapon
{
	GENERATED_BODY()

protected:

	virtual void Fire();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class ABaseProjectile> ProjectileClass;

	
	
};
