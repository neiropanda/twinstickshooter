// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUp_HealthPack.h"
#include "MyCharacter.h"
#include "Vitality/VitalityComponent.h"


void APickUp_HealthPack::OnPickUpHealth(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
//Super::OnPickUp(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	UVitalityComponent* VitalityComp = Cast<UVitalityComponent>(OtherActor);

	if (VitalityComp)
	{
		VitalityComp->MaxHealth = VitalityComp->CurrentHealth + HealthPoints;
		Destroy();
	}

}