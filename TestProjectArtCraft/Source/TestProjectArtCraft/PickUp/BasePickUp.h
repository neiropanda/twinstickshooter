// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BasePickUp.generated.h"

UCLASS()
class TESTPROJECTARTCRAFT_API ABasePickUp : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABasePickUp();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent* PickUpMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UShapeComponent* PickUpBox;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USceneComponent* PickUpRoot;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		class USoundBase* PickUpSound;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		class UParticleSystem* PickUpParticle;


	UFUNCTION(BlueprintCallable)
		virtual void SpawnEffects();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	virtual void OnPickUp(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	//virtual	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		//TSubclassOf<ABasePickUp> PickUpComponent_Class;
};
