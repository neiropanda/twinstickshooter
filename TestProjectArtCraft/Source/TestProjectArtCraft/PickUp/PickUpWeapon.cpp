// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUpWeapon.h"
#include "PickUp/BasePickUp.h"
#include "MyCharacter.h"
#include "Weapon/BaseWeapon.h"

void APickUpWeapon::OnPickUpWeapon(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnPickUp(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	AMyCharacter* MyChar = Cast<AMyCharacter>(OtherActor);
	if (MyChar->WeaponToSpawn_Class != PickUpWeapon_Class)
	{
		MyChar->WeaponToSpawn_Class = PickUpWeapon_Class;
		MyChar->DropWeapon();
		MyChar->CurrentWeapon = MyChar->SpawnWeapon();
		MyChar->AttachWeapon(MyChar->WeaponSocket);
	}
	/*else
	{
		MyChar->CurrentWeapon->CurrentClip = MyChar->CurrentWeapon->ClipSize;
		MyChar->CurrentWeapon->TotalAmmo = MyChar->CurrentWeapon->MaxAmmo;
		MyChar->CurrentWeapon->OnAmmoChanged.Broadcast();
	}*/
	

}

