// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUp/BasePickUp.h"
#include "PickUp_HealthPack.generated.h"

/**
 * 
 */
UCLASS()
class TESTPROJECTARTCRAFT_API APickUp_HealthPack : public ABasePickUp
{
	GENERATED_BODY()

public:

	UFUNCTION()
	void OnPickUpHealth(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health")
		float HealthPoints = 10;
};
