// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUp/BasePickUp.h"
#include "PickUpWeapon.generated.h"

/**
 * 
 */
UCLASS()
class TESTPROJECTARTCRAFT_API APickUpWeapon : public ABasePickUp
{
	GENERATED_BODY()

protected:

		UPROPERTY(EditDefaultsOnly, Category = "PickUp")
		TSubclassOf<class ABaseWeapon> PickUpWeapon_Class;

		UFUNCTION()
		void OnPickUpWeapon(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};
