// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ObjectMacros.h"
#include "MyCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FMyDelegate, bool, bMyBool);

class UCameraComponent;
class USpringArmComponent;
class UCharacterMovementComponent;

UCLASS()
class TESTPROJECTARTCRAFT_API AMyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveForward(float Scale);
	void MoveRight(float Scale);
	void Sprint();
	void StopSprint();
	void CameraZoom(float Scale);

	void RotateToMouse();

	void StartFire();
	void StopFire();

	virtual void Reload();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UVitalityComponent* VitalityComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USpringArmComponent* SpringArmComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCharacterMovementComponent* CharacterMovementComponent;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Speed Settings")
	bool bIsSprinting = false;

	UPROPERTY()
	bool bCanFire = false;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Speed Settings")
	float MaxWalkSpeed = 1200;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Speed Settings")
	float DefaultWalkSpeed = 600;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Settings")
	float CameraStep = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Settings")
	float MinCameraLenght = 800;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Settings")
	float MaxCameraLength = 1200;



	

public:	

	UPROPERTY(EditDefaultsOnly, Category = "Base Weapon")
		TSubclassOf<class ABaseWeapon> WeaponToSpawn_Class;

	UPROPERTY(EditDefaultsOnly, Category = "Base Weapon")
		FName WeaponSocket;

	UPROPERTY()
		class ABaseWeapon* CurrentWeapon;

	class ABaseWeapon* SpawnWeapon();
	void AttachWeapon(FName SocketName);

	void DropWeapon();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;



};
