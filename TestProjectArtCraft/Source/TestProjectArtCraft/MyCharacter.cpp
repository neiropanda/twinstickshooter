// Fill out your copyright notice in the Description page of Project Settings.


#include "MyCharacter.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/SceneComponent.h"
#include "ConstructorHelpers.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/Engine.h"
#include "RotationMatrix.h"
#include "Weapon/BaseWeapon.h"
#include "Vitality/VitalityComponent.h"

// Sets default values
AMyCharacter::AMyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AutoPossessPlayer = EAutoReceiveInput::Player0;

	VitalityComp = CreateDefaultSubobject<UVitalityComponent>(TEXT("VitalityComponent"));

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArmComponent->SetupAttachment(RootComponent);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(SpringArmComponent);

	SpringArmComponent->TargetArmLength = 1200;

	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SkelMesh(TEXT("SkeletalMesh'/Game/AdvancedLocomotionV3/Characters/Mannequin/Mannequin.Mannequin'"));

	if (SkelMesh.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SkelMesh.Object);
	}

	GetMesh()->AddLocalTransform(FTransform(FRotator(0, -90, 0).Quaternion(), FVector(0, 0, -90)));

	bUseControllerRotationYaw = false;
	SpringArmComponent->bInheritPitch = false;
	SpringArmComponent->bInheritRoll = false;
	SpringArmComponent->bInheritYaw = false;

}

void AMyCharacter::MoveForward(float Scale)
{
	if (!bIsSprinting)
	{
		AddMovementInput(FVector(1, 0, 0), Scale);
	}
	else
	{
		AddMovementInput(GetActorForwardVector(), Scale);
	}

}

void AMyCharacter::MoveRight(float Scale)
{
	AddMovementInput(FVector(0, 1, 0), Scale);

}

void AMyCharacter::Sprint()
{
	
	GetCharacterMovement()->MaxWalkSpeed = MaxWalkSpeed;
	bIsSprinting = true;
	bCanFire = false;
	GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Green, FString::SanitizeFloat(MaxWalkSpeed));
}

void AMyCharacter::StopSprint()
{
	GetCharacterMovement()->MaxWalkSpeed = DefaultWalkSpeed;
	bIsSprinting = false;
	bCanFire = true;
	GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Blue, FString::SanitizeFloat(DefaultWalkSpeed));
}

void AMyCharacter::CameraZoom(float Scale)

{
	if (Scale > 0 && SpringArmComponent->TargetArmLength - CameraStep >= MinCameraLenght)
	{
		SpringArmComponent->TargetArmLength -= CameraStep;
	}
	else if (Scale < 0 && SpringArmComponent->TargetArmLength + CameraStep <= MaxCameraLength)
	{
		SpringArmComponent->TargetArmLength += CameraStep;
	}

}

void AMyCharacter::RotateToMouse()
{
	FHitResult Hit;

	if (GetWorld()->GetFirstPlayerController()->GetHitResultUnderCursor(ECC_Visibility, false, Hit))
	{
		float RotationYaw = FRotationMatrix::MakeFromX(Hit.ImpactPoint - GetActorLocation()).Rotator().Yaw;
		SetActorRotation(FRotator(0, RotationYaw, 0));
		bIsSprinting = true;
	}

}

void AMyCharacter::StartFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StartFire();
	}
}

void AMyCharacter::StopFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
	}
}

void AMyCharacter::DropWeapon()
{
	CurrentWeapon->Destroy();
}

void AMyCharacter::Reload()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Reload();
	}
}




// Called when the game starts or when spawned
void AMyCharacter::BeginPlay()
{
	Super::BeginPlay();

	CurrentWeapon = SpawnWeapon();
	AttachWeapon(WeaponSocket);
	
}

ABaseWeapon* AMyCharacter::SpawnWeapon()
{

	if (WeaponToSpawn_Class)
	{
		FTransform TmpTransform = FTransform::Identity;
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;

		UE_LOG(LogTemp, Warning, TEXT("Trying to spawn weapon"));

		return Cast<ABaseWeapon>(GetWorld()->SpawnActor(WeaponToSpawn_Class, &TmpTransform, SpawnParams));
	}
	return nullptr;
}

void AMyCharacter::AttachWeapon(FName SocketName)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, SocketName);
	}
}

// Called every frame
void AMyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	RotateToMouse();

}

// Called to bind functionality to input
void AMyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AMyCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AMyCharacter::MoveRight);

	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Pressed, this, &AMyCharacter::Sprint);
	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Released, this, &AMyCharacter::StopSprint);

	PlayerInputComponent->BindAxis(TEXT("CameraZoom"), this, &AMyCharacter::CameraZoom);

	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &AMyCharacter::StartFire);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Released, this, &AMyCharacter::StopFire);

	PlayerInputComponent->BindAction(TEXT("Reload"), IE_Pressed, this, &AMyCharacter::Reload);
}

