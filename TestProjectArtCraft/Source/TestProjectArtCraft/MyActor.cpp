// Fill out your copyright notice in the Description page of Project Settings.


#include "MyActor.h"
#include "GameFramework/Actor.h"
#include "Engine/Engine.h"

// Sets default values
AMyActor::AMyActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();
	AMyActor::CheckTime();
	
}

void AMyActor::ActorMovement(float DeltaTime)
{
	float Distance = Step * DeltaTime;

	if (bUpDirection) {
		AddActorWorldOffset(FVector(0, 0, Distance));
		CurrentTime += DeltaTime;
		if (CurrentTime >= TimeToFly) {
			bUpDirection = false;
		}
	}
	else {
		AddActorWorldOffset(FVector(0, 0, Distance * -1));
		CurrentTime -= DeltaTime;
		if (CurrentTime <= 0) {
			bUpDirection = true;
		}
	}
}

void AMyActor::CheckTime()
{

	FCollisionQueryParams Params;
	FHitResult Hit;


	double StartTime = FPlatformTime::Seconds();

	for (float Size = 0; Size <= 1000000; Size++)
	{
		GetWorld()->LineTraceSingleByChannel(Hit, GetActorLocation(), GetActorLocation() + (GetActorLocation().GetSafeNormal() * 500), ECC_Visibility, Params);
	}

	double EndTime = FPlatformTime::Seconds();
	double SearchTime = EndTime - StartTime;

	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::SanitizeFloat(SearchTime));

}

// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AMyActor::ActorMovement(DeltaTime);
	
}


