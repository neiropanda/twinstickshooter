// Fill out your copyright notice in the Description page of Project Settings.


#include "VitalityComponent.h"
#include "TimerManager.h"
#include "Engine/Engine.h"

// Sets default values for this component's properties
UVitalityComponent::UVitalityComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UVitalityComponent::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwner())
	{
		GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UVitalityComponent::DamageHandle);
	}
	
}

void UVitalityComponent::StartRegen()
{
	GetOwner()->GetWorldTimerManager().SetTimer(RegenTime_TH, this, &UVitalityComponent::Regeneration, RegenPerSec, true, RegenDelay);
}

void UVitalityComponent::StopRegen()
{
	GetOwner()->GetWorldTimerManager().ClearTimer(RegenTime_TH);
}

void UVitalityComponent::Regeneration()
{
	if (CurrentHealth < MaxHealth && CurrentHealth > 0)
	{
		CurrentHealth += RegenPerSec;
		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Emerald, TEXT("Current Health is ") + FString::FromInt(CurrentHealth));

		OnHeathChanged.Broadcast();
	}
	else
	{
		StopRegen();
	}

}

void UVitalityComponent::DamageHandle(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (CurrentHealth <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("Already Dead"));
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("%s was damaged for %s"), *DamagedActor->GetName(), *FString::SanitizeFloat(Damage));

	CurrentHealth -= Damage;

	OnHeathChanged.Broadcast();

	if (CurrentHealth <= 0)
	{
		OnOwnerDied.Broadcast();
		UE_LOG(LogTemp, Warning, TEXT("Owner Died"));
	}
	StartRegen();
}

/*void UVitalityComponent::AddHealth(float HealthToAdd)
{
	if (CurrentHealth > 0 && CurrentHealth != MaxHealth)
	{
		float NewHealth = CurrentHealth + HealthToAdd;
		if (NewHealth < MaxHealth)
		{
			CurrentHealth = NewHealth;
		}
		else
		{
			CurrentHealth = MaxHealth;
		}
	}
}*/




// Called every frame
void UVitalityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

