// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VitalityComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoMyParamDelegate);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TESTPROJECTARTCRAFT_API UVitalityComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UVitalityComponent();

	// Called when the game starts
	virtual void BeginPlay() override;

	void StartRegen();
	void StopRegen();

	UFUNCTION()
		void Regeneration();

	FTimerHandle RegenTime_TH;

	UFUNCTION()
		void DamageHandle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Health")
		float MaxHealth = 100;

	UPROPERTY(BlueprintReadOnly, Category = "Health")
		float CurrentHealth = MaxHealth;

	UPROPERTY(BlueprintReadOnly, Category = "Health")
		float RegenPerSec = 1;

	UPROPERTY(BlueprintReadOnly, Category = "Health")
		float RegenDelay = 2;

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNoMyParamDelegate OnHeathChanged;

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNoMyParamDelegate OnOwnerDied;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	//UFUNCTION()
		//void AddHealth(float HealthToAdd);
};